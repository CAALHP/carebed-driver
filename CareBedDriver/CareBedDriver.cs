﻿using System.AddIn;
using Plugins.DriverPluginAdapters;

namespace Plugins.CareBedDriver
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("CareBed Driver Plugin", Version = "1.0.0.0")]
    public class CareBedDeviceDriver : DeviceDriverViewPluginAdapter
    {
        public CareBedDeviceDriver()
        {
            DeviceDriver = new CareBedDeviceDriverImplementation();
            
        }
    }
}