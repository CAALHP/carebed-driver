﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Timers;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using Plugins.CareBedDriver.BedWeightMonitorServiceReference;

namespace Plugins.CareBedDriver
{
    public class CareBedDeviceDriverImplementation : IDeviceDriverCAALHPContract
    {
        private IDeviceDriverHostCAALHPContract _host;
        private int _processId;
        private BedWeightSensorClient _client;
        private Timer _timer;

        public double GetMeasurement()
        {
            var rnd = new System.Random();
            return rnd.NextDouble();
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            //throw new System.NotImplementedException();
        }

        public string GetName()
        {
            return "CareBedDriver";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            _client = new BedWeightSensorClient();
            _timer = new Timer(10000) { AutoReset = true };
            _timer.Elapsed += TimerOnElapsed;
            _timer.Start();

            var initEvent = new InitializedEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                Name = GetName()
            };
            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, initEvent);
            _host.Host.ReportEvent(serializedEvent);
            //_host.Host.ReportEvent(EventHelper.CreateEvent(_processId, GetName(), "Started CareBedDriver"));

        }

        private async void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {

            try
            {
                var weight = await _client.WeightAsync();
                if (!(weight > 40.0)) return;
                var weightEvent = new SimpleMeasurementEvent
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    MeasurementType = "Weight",
                    Value = weight
                };
                var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, weightEvent);
                _host.Host.ReportEvent(serializedEvent);
                //_host.Host.ReportEvent(EventHelper.CreateEvent("Weight", weight)); 
            }
            catch (Exception e)
            {
                _timer.Stop();
                var errorEvent = new ErrorEvent
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    ErrorMessage = e.Message
                };
                var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, errorEvent);
                _host.Host.ReportEvent(serializedEvent);
                //_host.Host.ReportEvent(EventHelper.CreateEvent(_processId, GetName(), "Error", e.Message));
            }
        }
    }
}